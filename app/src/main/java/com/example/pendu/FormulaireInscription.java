package com.example.pendu;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class FormulaireInscription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulaire_inscription);
    }

    @Override
    public void finish(){
        super.finish();
    }

    public void fin(View view){
        EditText e = findViewById(R.id.FormulairePseudo);
        EditText e2 = findViewById(R.id.FormulaireMdp);
        EditText e3 = findViewById(R.id.FormaulaireConfirmMdp);
        if(e.getText().toString().equals("") || e2.getText().toString().equals("") || e3.getText().toString().equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Champs non rempli");
            builder.setMessage("Vous n'avez pas rempli tous les champs requis");
            builder.setPositiveButton("Confirmer",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else{
            finish();
        }
    }

    public void fin2(View view){
        finish();
    }
}
