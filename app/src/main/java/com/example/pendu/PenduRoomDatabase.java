package com.example.pendu;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;



@Database(entities = {Player.class, Game.class}, version = 1,exportSchema = false)
public abstract class PenduRoomDatabase extends RoomDatabase {

    public abstract  GameDao gameDao();
    public abstract  PlayerDao playerDao();

    private  static  PenduRoomDatabase INSTANCE;

    static PenduRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (PenduRoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PenduRoomDatabase.class,"pendu_database").build();
                }
            }
        }
        return INSTANCE;
    }
}
