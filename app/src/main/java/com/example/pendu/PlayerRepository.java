package com.example.pendu;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class PlayerRepository {
    private PlayerDao playerDao;
    private LiveData<List<Player>> mAllPlayers;

    PlayerRepository(Application application){
        PenduRoomDatabase db = PenduRoomDatabase.getDatabase(application);
        db.playerDao();
        mAllPlayers = playerDao.getAllPlayers();
    }

    LiveData<List<Player>> getAllPlayers(){
        return mAllPlayers;
    }

    void insert(Player player){
        new InsertAsyncTask(playerDao).execute(player);
    }

    private  static class InsertAsyncTask extends AsyncTask<Player,Void,Void> {
        private  PlayerDao mAsyncTaskDao;

        InsertAsyncTask(PlayerDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Player... params){
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public Integer getNbElements(){
        try {
            return new GetNbElementsAsync(playerDao).execute().get();
        }catch (Exception e){
            Log.d("","");
        }
        return null;
    }

    private static class GetNbElementsAsync extends AsyncTask<Void,Void,Integer>{
        private PlayerDao mplayerDao;

        GetNbElementsAsync(PlayerDao playerDao){
            mplayerDao = playerDao;
        }

        @Override
        protected Integer doInBackground(Void... params){
            return mplayerDao.nbElements();
        }
    }
}
