package com.example.pendu;

import android.os.Parcel;
import android.os.Parcelable;

public class PartieJeu implements Parcelable {
    private String nom;

    public PartieJeu(String nom){
        this.nom=nom;
    }

    public PartieJeu(Parcel parcel){
        this.nom=parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel dest,int flags){
        dest.writeString(nom);
    }

    @Override
    public int describeContents(){return 0;}

    public static Creator<PartieJeu> CREATOR = new Creator<PartieJeu>() {
        @Override
        public PartieJeu createFromParcel(Parcel parcel){return new PartieJeu(parcel); }


        @Override
        public PartieJeu[] newArray(int i) { return new PartieJeu[i]; }

    };

    public String getNom(){ return nom;}
}

