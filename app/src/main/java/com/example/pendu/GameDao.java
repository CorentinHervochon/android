package com.example.pendu;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface GameDao {
    @Query("SELECT * FROM game_table")
    LiveData<List<Game>> getAllGames();

    @Query("SELECT * FROM game_table WHERE idPlayer IN (:userIdPs)")
    LiveData<List<Game>> loadAllByIdPs(int[] userIdPs);

    @Query("SELECT * FROM game_table WHERE idG LIKE :idg ")
    Game findByIdG(int idg);

    @Insert
    void insert(Game game);

    @Query("SELECT count(*) FROM game_table")
    int nbElements();

    @Query("DELETE FROM game_table")
    void delete_all();
}
