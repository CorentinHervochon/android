package com.example.pendu;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;


@Entity(indices = {@Index("idPlayer")},tableName = "game_table", foreignKeys = @ForeignKey(entity = Player.class, parentColumns = "idP", childColumns = "idPlayer"))
public class Game {
    @PrimaryKey
    @ColumnInfo(name = "idG")
    public final int idG;

    @ColumnInfo(name = "idPlayer")
    public final int idPlayer;

    @ColumnInfo(name = "score")
    public int score;

    Game(int idG, final int idPlayer, int score) {
        this.idG = idG;
        this.idPlayer = idPlayer;
        this.score = score;
    }
}
