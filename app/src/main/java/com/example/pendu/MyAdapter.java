package com.example.pendu;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private static ArrayList<PartieJeu> lesParties;

    public MyAdapter(ArrayList<PartieJeu> list){
        lesParties = list;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView textViewNomP;



        private PartieJeu current;

        public MyViewHolder(final View v){
            super(v);
            textViewNomP = v.findViewById(R.id.textViewNomP);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent;
                    Context context = v.getContext();
                    switch (getAdapterPosition()) {
                        default :
                            intent = new Intent(context, Jeu.class);
                            context.startActivity(intent);
                            break;
                    }
                }

            });

        }


        public void display(PartieJeu p){
            current = p;
            textViewNomP.setText(p.getNom());
        }
    }



    //Create new views (invoked by Layout Manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view  = layoutInflater.inflate(R.layout.cell,parent,false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    //Replace the contents of a View (invoked by Layout Manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder,int position){
        //chercher la partie a la position dans la liste
        //remplacer le contenu de la view par cette partie

        PartieJeu partie = lesParties.get(position);
        holder.display(partie);
    }

    @Override
    public int getItemCount(){
        return lesParties.size();
    }

    public void setLesParties(ArrayList<PartieJeu> p) {lesParties = p;}
}
