package com.example.pendu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("test", "onCreate: ");
    }

    public void lancerFormulaireInscription(View view){
        Intent intent = new Intent(this,FormulaireInscription.class);
        startActivityForResult(intent, 0);
    }

    public void lancerHome(View view){
        //FAIRE DES CONDITIONS AVEC LA BD SI LA PERSONNE EXISTE ET LES BONS IDENTIFIANTS !!!
        Log.d("NTM", "lancerHome: NTM");
        Intent intent = new Intent(this,Home.class);
        startActivityForResult(intent, 0);
    }
}

