package com.example.pendu;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class GameRepository {
    private GameDao gameDao;
    private LiveData<List<Game>> mAllGames;

    GameRepository(Application application){
        PenduRoomDatabase db = PenduRoomDatabase.getDatabase(application);
        db.gameDao();
        mAllGames = gameDao.getAllGames();
    }

    LiveData<List<Game>> getAllGames(){
        return mAllGames;
    }

    void insert(Game game){
        new InsertAsyncTask(gameDao).execute(game);
    }

    private  static class InsertAsyncTask extends AsyncTask<Game,Void,Void> {
        private  GameDao mAsyncTaskDao;

        InsertAsyncTask(GameDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Game... params){
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public Integer getNbElements(){
        try {
            return new GetNbElementsAsync(gameDao).execute().get();
        }catch (Exception e){
            Log.d("","");
        }
        return null;
    }

    private static class GetNbElementsAsync extends AsyncTask<Void,Void,Integer>{
        private GameDao mgameDao;

        GetNbElementsAsync(GameDao gameDao){
            mgameDao = gameDao;
        }

        @Override
        protected Integer doInBackground(Void... params){
            return mgameDao.nbElements();
        }
    }

    public void deleteAll(){
        new DeleteAllAsyncTask(gameDao).execute();
    }

    private static  class  DeleteAllAsyncTask extends  AsyncTask<Void,Void,Void>{
        private GameDao mAsyncTaskDao;

        DeleteAllAsyncTask(GameDao gameDao){
            mAsyncTaskDao = gameDao;
        }

        @Override
        protected Void doInBackground(Void... params){
            mAsyncTaskDao.delete_all();
            return null;
        }
    }
}
