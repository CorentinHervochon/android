package com.example.pendu;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class GameViewModel extends AndroidViewModel {
    private GameRepository mRepository;

    private LiveData<List<Game>> mAllGame;

    public GameViewModel (Application application) {
        super(application);
        mRepository = new GameRepository(application);
        mAllGame = mRepository.getAllGames();
    }

    LiveData<List<Game>> getmAllGames() { return mAllGame; }

    public void insert(Game game) { mRepository.insert(game); }
}
