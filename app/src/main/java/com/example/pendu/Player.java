package com.example.pendu;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "player_table")
public class Player {
    @PrimaryKey
    @ColumnInfo(name = "idP")
    public int idP;

    @NonNull
    @ColumnInfo(name = "login")
    public String login;

    @NonNull
    @ColumnInfo(name = "mdp")
    public String mdp;

    Player(int idP, @NonNull String login, @NonNull String mdp) {
        this.idP = idP;
        this.login = login;
        this.mdp = mdp;
    }
}
