package com.example.pendu;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class PlayerViewModel extends AndroidViewModel {

    private PlayerRepository mRepository;

    private LiveData<List<Player>> mAllPlayers;

    public PlayerViewModel (Application application) {
        super(application);
        mRepository = new PlayerRepository(application);
        mAllPlayers = mRepository.getAllPlayers();
    }

    LiveData<List<Player>> getmAllPlayers() { return mAllPlayers; }

    public void insert(Player player) { mRepository.insert(player); }
}