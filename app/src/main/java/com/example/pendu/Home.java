package com.example.pendu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

public class Home extends AppCompatActivity {

    private ArrayList<PartieJeu> lesParties = new ArrayList<PartieJeu>();
    private MyAdapter myAdapter;
    private LinearLayoutManager linearLayoutManager;
    private Parcelable recyclerState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        lesParties.add(new PartieJeu("Test"));

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        myAdapter = new MyAdapter(lesParties);
        recyclerView.setAdapter(myAdapter);

        //Récupération du fichier de préférences par défaut
        SharedPreferences settings = getPreferences(MODE_PRIVATE);

        //Récupération de la chaîne associé à la clé nommé "text"
        //SI la clé n'existe pas, la chaîne "empty" est retournée
        String txt = settings.getString("text","empty");

        //La chaîne récupérée est mise dans une view pour affichage
        /*EditText editText = findViewById(R.id.HomeNomPartie);
        editText.setText(txt);*/
    }

    @Override
    protected void onStop(){

        EditText editText = findViewById(R.id.HomeNomPartie);
        String txt = editText.getText().toString();

        SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
        editor.putString("text", txt);
        editor.commit();
        super.onStop();
    }
    public void ajouter(View view){
        EditText editTextNom = findViewById(R.id.HomeNomPartie);


        PartieJeu p = new PartieJeu(editTextNom.getText().toString());
        lesParties.add(p);
        myAdapter.notifyDataSetChanged();
    }

    protected void onSaveInstanceSate(Bundle state){
        super.onSaveInstanceState(state);
        //sauvegarde de la position dans la recyclerView
        recyclerState = linearLayoutManager.onSaveInstanceState();
        state.putParcelable("infos",recyclerState);
        //sauvegarde des infos
        state.putParcelableArrayList("donnees",lesParties);
    }

    protected void onRestaureInstanceState(Bundle state){
        super.onRestoreInstanceState(state);
        if (state!=null){
            recyclerState = state.getParcelable("infos");
            lesParties = state.getParcelableArrayList("donnees");
            myAdapter.setLesParties(lesParties);
            myAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (recyclerState!=null){
            linearLayoutManager.onRestoreInstanceState(recyclerState);
        }
        myAdapter.notifyDataSetChanged();
    }

    @Override
    public void finish(){
        super.finish();
    }

    public void fin3(View view){
        finish();
    }
}

