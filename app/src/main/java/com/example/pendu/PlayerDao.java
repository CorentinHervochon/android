package com.example.pendu;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PlayerDao {
    @Query("SELECT * FROM player_table")
    LiveData<List<Player>> getAllPlayers();

    @Query("SELECT * FROM player_table WHERE idP IN (:userIds)")
    LiveData<List<Player>> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM player_table WHERE login LIKE :login ")
    Player findByLogin(String login);

    @Insert
    void insert(Player player);

    @Query("SELECT count(*) FROM player_table")
    int nbElements();

}
